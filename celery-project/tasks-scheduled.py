from celery import Celery
from celery.schedules import crontab

app = Celery()

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(5.0, test.s('hello'))

    sender.add_periodic_task(6.0, test.s('world'))

@app.task
def test(arg):
    print(arg)
