import requests
from celery import Celery, group

# app = Celery()

# Can use this to enable and check results
app = Celery(backend='rpc://')

# The last request will time out
urls = ['http://www.google.com', 'http://www.stackoverflow.com',
        'http://www.gitlab.com', 'http://www.github.com',
        'http://www.amazon.com', 'http://www.npr.com',
        'http://www.ebay.com', 'http://www.cnn.com',
        'http://www.meetup.com', 'http://www.bbc.com',
        'http://10.255.255.1']

@app.task
def fetch_sites():
    """Fetch urls a few times"""
    result = {}

    for url in urls:
        result[url] = []

    for _ in range(3):
        for url in urls:
            try:
                response = requests.get(url, timeout=5)
                result[url].append(str(response.status_code))
            except requests.exceptions.ConnectTimeout:
                result[url].append('Timeout!')

            print('{}: {}'.format(url, result[url]))

    return result


def fetch_sites_concurrent():
    """Dispatches three sets of requests to urls"""
    for _ in range(3):
        for i in range(len(urls)):
            fetch_site.delay(i)


def fetch_sites_chained():
    """All other urls wait on a call to the last one"""
    dependent = group(fetch_site.si(x) for x in range(len(urls)-1))
    (fetch_site.s(len(urls)-1) | dependent)()


@app.task
def fetch_site(i):
    try:
        response = requests.get(urls[i], timeout=5)
        result = str(response.status_code)
    except requests.exceptions.ConnectTimeout:
        result = 'Timeout!'

    print('{}: {}'.format(urls[i], result))
