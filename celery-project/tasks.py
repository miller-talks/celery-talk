from celery import Celery

app = Celery()

@app.task
def add(x, y):
    print(x + y)
